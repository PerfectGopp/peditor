let cursor = {}, config = {};
let isff = navigator.userAgent.toLocaleLowerCase().indexOf('firefox') >= 0 ? true : false;
//加载iconfont
function loadFont() {
	let link = document.createElement('link');
	link.rel = 'stylesheet';
	link.href = '//at.alicdn.com/t/font_1783847_0xpbeodfpzcg.css';
	document.querySelector('head').appendChild(link);
}
function loadCSS() {
	let base = import.meta.url;
	let path = base.substr(0, base.lastIndexOf('/')) + '/peditor.css';
	let link = document.createElement('link');
	link.rel = 'stylesheet';
	link.href = path;
	document.querySelector('head').appendChild(link);
}
//绘制editor
function draw(container, order) {
	let main =document.createElement('div');
	main.className = "peditor";
	main.id = 'peditor';
	main.style.width = getComputedStyle(container, null).width;
	main.style.height = getComputedStyle(container, null).height;
	container.appendChild(main);
	let menu = document.createElement('div');
	menu.className = 'peditor-menu';
	main.appendChild(menu);
	let list = order ? order : ['bold', 'italic', 'underline', 'color', 'background', 'header', 'left', 'center', 'right', 'justify', 'ordered', 'unordered', 'link', 'image', 'line', 'table', 'save'];
	let icon = new Map([
		['bold', 'icon-bold'],
		['italic' , 'icon-italic'],
		['underline', 'icon-underline'],
		['left' , 'icon-align-left'],
		['center' , 'icon-align-center'],
		['right' , 'icon-align-right'],
		['justify' , 'icon-align-justify'],
		['image', 'icon-image'],
		['link', 'icon-link'],
		['ordered', 'icon-ordered-list'],
		['unordered', 'icon-unordered-list'],
		['line', 'icon-line'],
		['table', 'icon-table'],
		['save' , 'icon-save'],
		['header', 'icon-header'],
		['color', 'icon-font-color'],
		['background', 'icon-background-color']
	]);
	let tl = new Map([
		['bold', '粗体'],
		['italic' , '斜体'],
		['underline', '下划线'],
		['left' , '左对齐'],
		['color', '文字颜色'],
		['background', '背景色'],
		['center' , '居中对齐'],
		['right' , '右对齐'],
		['justify' , '两端对齐'],
		['image', '图片'],
		['link', '链接'],
		['ordered', '有序列表'],
		['unordered', '无序列表'],
		['line', '水平线'],
		['table', '表格'],
		['save' , '保存']
	]);
	list.forEach(item => {
		let span = document.createElement('span');
		span.className = `peditor-order iconfont ${icon.get(item)}`;
		span.id = `peditor-order-${item}`;
		if(tl.has(item)) span.setAttribute('title', tl.get(item));
		menu.appendChild(span);
		addHandle(span, item);
	})
	let edit = document.createElement('div');
	let h = getComputedStyle(container, null)['height'];
	h = h.substr(0, h.length - 2) - 40 + 'px';
	edit.setAttribute('contenteditable', true);
	edit.className = 'peditor-body';
	edit.id = 'peditor-body';
	edit.addEventListener('keyup', getSelect);
	edit.addEventListener('mouseup', getSelect);
	main.appendChild(edit);
}
function getElemFromPoint() {
	let e = event;　
	let x = e.clientX + document.body.scrollLeft;
	let y = e.clientY + document.body.scrollTop;
	return document.elementFromPoint(x, y);
}
function addHandle(elem, handle) {
	switch(handle) {
		case 'bold' :
			elem.addEventListener('click', () => cmd('bold'));
			break;
		case 'italic' :
			elem.addEventListener('click', () => cmd('italic'));
			break;
		case 'underline' :
			elem.addEventListener('click', () => cmd('underline'));
			break;
		case 'left' :
			elem.addEventListener('click', () => cmd('justifyLeft'));
			break;
		case 'center' :
			elem.addEventListener('click', () => cmd('justifyCenter'));
			break;
		case 'right' :
			elem.addEventListener('click', () => cmd('justifyRight'));
			break;
		case 'justify' :
			elem.addEventListener('click', () => cmd('justifyFull'));
			break;
		case 'ordered' :
			elem.addEventListener('click', () => insertList('ol'));
			break;
		case 'unordered' :
			elem.addEventListener('click', () => insertList('ul'));
			break;
		case 'link' :
			elem.addEventListener('click', setLink);
			break;
		case 'image' :
			elem.addEventListener('click', insertImage);
			break;
		case 'table' :
			elem.addEventListener('click', insertTable);
			break;	
		case 'line' :
			elem.addEventListener('click', insertLine);
			break;
		case 'save' :
			elem.addEventListener('click', save);
			break;
		case 'header' :
			elem.addEventListener('mouseenter', setHeader);
			elem.addEventListener('mouseleave', function() {
				let dom = document.getElementById("peditor-ul");
				let now = getElemFromPoint();
				if(dom !== null && (!now.hasAttribute('id') || now.id != 'peditor-ul')) {
					dom.parentNode.removeChild(dom);
				}
			});
			break;
		case 'color' :
			elem.addEventListener('click', setColor);
			break;
		case 'background' :
			elem.addEventListener('click', () => {setColor('bg')});
			break;
	}
}
function getSelect() {
	let sel = document.getSelection()
	cursor.start = sel.anchorOffset;
	cursor.snode = sel.anchorNode;
	cursor.end = sel.focusOffset;
	cursor.enode = sel.focusNode;
}
function isCollapse() {
	if(cursor.start === 'undefined' || (cursor.start == cursor.end && cursor.snode === cursor.enode)) {
		return true;
	} else {
		return false;
	}
}
function selected() {
	if(typeof cursor.start === 'undefined') {
		document.getElementById('peditor-body').focus();
	} else {
		let range = document.createRange();
		range.setStart(cursor.snode, cursor.start);
		range.setEnd(cursor.enode, cursor.end);
		let sel = getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
	}	
}
function popup(title, body, handle) {
	let parent = document.getElementById('peditor');
	let cover = document.createElement('div');
	cover.className = 'peditor-popup-cover';
	parent.appendChild(cover);
	let container = document.createElement('div');
	container.className = 'peditor-popup';
	parent.appendChild(container);
	let tl = document.createElement('div');
	tl.innerHTML = title;
	tl.className = 'peditor-popup-title';
	container.appendChild(tl);
	container.innerHTML += body;
	let footer = document.createElement('div');
	footer.className = 'peditor-popup-footer';
	footer.innerHTML = `
		<button id="peditor-btn-submit">完成</button>
		<button id="peditor-btn-cancel">放弃</button>
	`;
	container.appendChild(footer);
	document.getElementById('peditor-btn-cancel').addEventListener('click', destoryPopup);
	document.getElementById('peditor-btn-submit').addEventListener('click', handle);
}
function destoryPopup() {
	let popup = document.querySelector('.peditor-popup');
	popup.parentNode.removeChild(popup);
	let cover = document.querySelector('.peditor-popup-cover');
	cover.parentNode.removeChild(cover);
}
function tips(msg) {
	let parent = document.getElementById('peditor');
	let tip = document.createElement('div');
	tip.innerHTML = msg;
	tip.id = 'peditor-tips';
	tip.className = 'peditor-tips';
	document.getElementById('peditor').appendChild(tip);
	let pw = getComputedStyle(parent, null).width;
	pw = pw.substr(0, pw.length - 2);
	let tw =  getComputedStyle(tip, null).width;
	tw = tw.substr(0, tw.length - 2);
	let left = (pw - tw) / 2 + 'px';
	tip.style.left = left;
	setTimeout(()=>{
		tip.parentNode.removeChild(tip);
	}, 3000);
}
function upload(url, param, callback) {
	let xhr = new XMLHttpRequest();
	let data = new FormData();
	for(let k in param) {
		data.append(k, param[k]);
	}
	xhr.open('post', url, true);
	xhr.responseType = 'json';
	xhr.send(data);
	xhr.onreadystatechange = ()=>{
		if(xhr.readyState == 4 && xhr.status == 200) {
			if(callback !== undefined) (callback)(xhr.response);
		}
	}
}
function cmd(command, value = null) {
	selected();
	document.execCommand(command, false, value);
	getSelect();
}
function setParagraphSeparator(tag) {
	let control = document.getElementById('peditor-body');
	control.focus();
	cmd('defaultParagraphSeparator', tag);
	if(navigator.userAgent.toLowerCase().indexOf('firefox') == '-1') cmd('insertHTML', `<${tag}><br></${tag}>`);
}
function insertList(type) {
	let html = isff ? `<${type}><li></li></${type}>` : `<${type}><li><br></li></${type}>`;
	cmd('insertHTML', html);
}
function setColor(mode = '') {
	let ipt = document.createElement('input');
	ipt.type = 'color';
	ipt.click();
	mode == 'bg' 
	? ipt.addEventListener('change', () => cmd('backColor', ipt.value))
	: ipt.addEventListener('change', () => cmd('foreColor', ipt.value));
}
function setLink() {
	let flag = isCollapse();
	let body = '';
	if(flag) {
		body += `
			<div class="peditor-popup-item">
				<label for="url">标题</label>
				<input type="text" name="peditor-link-title" class="peditor-popup-ipt" id="peditor-link-title">
			</div>
		`;
	}
	body += `
		<div class="peditor-popup-item">
			<label for="url">链接</label>
			<input type="url" name="peditor-link-url" class="peditor-popup-ipt" id="peditor-link-url">
		</div>
	`;
	popup('设置链接', body, ()=>{
		let link = document.getElementById('peditor-link-url').value;
		selected();
		if(link == '') {
			tips('输入不完整，请检查！');
		} else {
			if(flag) {
				let tl = document.getElementById('peditor-link-title').value;
				if(tl =='') {
					tips('输入不完整，请检查！');
				} else {
					let html = `<a href="${link}">${tl}</a>`
					cmd('insertHTML', html);
					destoryPopup();
				}
			} else {
				cmd('createLink', link);
				destoryPopup();
			}
		}
	});
}
function insertImage() {
	let body = `
		<div class="peditor-popup-item">
			<input type="file" name="nativeimg" accept="image/*" id="nativeimg">
		</div>
		<div class="peditor-popup-item">
			<label for="url">链接</label>
			<input type="text" name="url" class="peditor-popup-ipt">
		</div>
	`;
	popup('插入图片', body, ()=>{
		let w = '';
		let h = ''; 
		let url = document.querySelector('[name=url]').value;
		if(url == '') {
			tips('链接不能为空！');
		} else {
			let html = `<div><img src="${document.querySelector('[name=url]').value}" style="max-width:calc(100% - 20px)"></div>`;
			html += isff ? '<p></p>' : '<p><br></p>';
			cmd('insertHTML', html);
			destoryPopup();
		}
	});
	let icontrol = document.getElementById('nativeimg')
	icontrol.addEventListener('change', ()=> {
		let data = {}
		data.source = icontrol.files[0];
		if(config.imgparam !== 'undefined') {
			for(let k in config.imgparam) {
				data.k = config.imgparam[k];
			}
		}
		upload(config.imgurl, data, (response, header)=> {
			if(response.status === true || typeof response.status === 'undefined') {
				document.querySelector('[name=url]').value = response.url;
			} else {
				icontrol.value = '';
				let msg = typeof response.error === 'undefined' ? '上传失败，请重新上传' : response.error;
				tips(msg);
			}
		});
					
	});
}
function insertTable() {
	let body = `
		<div class="peditor-popup-item">
			<label for="cols">行数</label>
			<input type="number" name="cols" class="peditor-popup-num" id="peditor-table-cols" required>
			<label for="rows">列数</label>
			<input type="number" name="rows" class="peditor-popup-num" id="peditor-table-rows" required>
		</div>
	`;
	popup('插入表格', body, ()=> {
		let c = document.getElementById('peditor-table-cols').value;
		let r = document.getElementById('peditor-table-rows').value;
		if(c != '' && r != '') {
			let html = '<table width="100%">';
			for(let cols = 0; cols < c; cols++) {
				html += '<tr>';
				for(let rows = 0; rows < r; rows++) {
					html += '<td></td>';
				}
				html += '</tr>';
			}
			html += '</table>';
			html += isff ? '<p></p>' : '<p><br></p>';
		cmd('insertHTML', html);
		destoryPopup()
		} else {
			tips('行数或列数未输入，请检查！');
		}
	});
}
function insertLine() {
	let html = isff ? '<hr><p></p>' : '<hr><p><br></p>';
	cmd('insertHTML', html);
}
function setHeader() {
	if(document.getElementById('peditor-ul') === null) {
		let l = document.getElementById('peditor-order-header').offsetLeft;
		let t = document.getElementById('peditor-order-header').offsetTop;
		let ul = document.createElement('ul');
		ul.className = 'peditor-ul';
		ul.id = 'peditor-ul';
		ul.style.cssText = `top:${t + 28}px; left:${l}px`;
		document.getElementById('peditor').appendChild(ul);
		let header = ['H1', 'H2', 'H3', 'H4', 'H5', 'H6'];
		header.forEach(item => {
			let li = document.createElement('li');
			li.className = 'peditor-li';
			li.setAttribute('value', item);
			li.innerHTML = `<${item}>${item}</${item}>`;
			li.addEventListener('click', () => {
				cmd('formatBlock', item);
				ul.parentNode.removeChild(ul);
			});
			ul.appendChild(li);
		})
		ul.addEventListener('mouseleave', ()=>{
			let dom = getElemFromPoint();
			if(!dom.hasAttribute('id') || dom.id != 'peditor-order-header') {
				ul.parentNode.removeChild(ul);
			}
		});
	}
}
function save() {
	if(config.hasOwnProperty('save')) {
		if(typeof config.save === 'function') {
			(config.save)();
		}
	}
}
export const version = '1.2.0';
export function init(opt = {}) {
	loadCSS();
	loadFont();
	let container = opt.hasOwnProperty('elem') && document.querySelector(opt.elem) !== null ? document.querySelector(opt.elem) : document.querySelector('body');
	opt.hasOwnProperty('menu') ? draw(container, opt.menu) : draw(container);	
	setParagraphSeparator('p');
	if(opt.hasOwnProperty('imgurl')) config.imgurl = opt.imgurl;
	if(opt.hasOwnProperty('imgparam')) config.imgparam = opt.imgparam;
	if(opt.hasOwnProperty('save')) config.save = opt.save;
	isff ? cmd('insertHTML', '<p></p>') : cmd('insertHTML', '<p><br></p>');
}
export function getContents(type = 'html') {
	if(type == 'html') {
		return document.getElementById('peditor-body').innerHTML;
	} else if(type == 'txt') {
		return document.getElementById('peditor-body').innerText;
	}
}