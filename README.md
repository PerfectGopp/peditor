# peditor

#### 由于MDN已提示execCommand已废弃，该编译器暂不维护
#### 介绍
一款简单而不平凡的富文本编译器

#### 快速使用
温馨提示：peditor未对IE兼容性测试，请需要支持IE浏览器的谨慎使用！！！

```
<script>
import('./src/pedirot.js')
.then(editor => {
    editor.init({
        elem : string，编译器容器，使用合理的css选择器，可选项
        imgurl : string, 图片上传地址，可选项，使用本地图片为必须项
        imgparam : json，图片上传额外参数, 可选项
        save : function{, 保存时操作函数, 可选项
            let html = editor.getContents(type = 'html') //返回值：string, 获取编译器内容，参数可选，参考值为'html','txt'
            console.log(html);
        }
    });
});
</script>
```

#### 效果图

![效果图](https://images.gitee.com/uploads/images/2020/0429/164013_c18e1dbe_1470288.png "效果图.png")
![效果图_插入图片](https://images.gitee.com/uploads/images/2020/0429/164039_27a33af2_1470288.png "效果图1.png")
![效果图_设置链接](https://images.gitee.com/uploads/images/2020/0429/164111_338bf510_1470288.png "效果图2.png")

目前该版本为测试版本，欢迎大家拍砖，交流进群：680294103 :boom: 